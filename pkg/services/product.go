package services

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/azonnix/authentication/product-svc/pkg/db"
	"gitlab.com/azonnix/authentication/product-svc/pkg/models"
	"gitlab.com/azonnix/authentication/product-svc/pkg/pb"
)

type Server struct {
	H db.Handler
	pb.UnimplementedProductServiceServer
}

func (s *Server) CreateProduct(ctx context.Context, req *pb.CreateProductRequest) (*pb.CreateProductResponse, error) {
	var product models.Product

	product.Name = req.Name
	product.Stock = req.Stock
	product.Price = req.Price

	if result := s.H.DB.Create(&product); result.Error != nil {
		return &pb.CreateProductResponse{
			Status: http.StatusConflict,
			Error:  result.Error.Error(),
		}, result.Error
	}

	return &pb.CreateProductResponse{
		Status: http.StatusCreated,
		Id:     product.Id,
	}, nil
}

func (s *Server) FindOne(ctx context.Context, req *pb.FindOneRequest) (*pb.FindOneResponse, error) {
	var product models.Product

	if result := s.H.DB.First(&product, req.Id); result.Error != nil {
		return &pb.FindOneResponse{
			Status: http.StatusNotFound,
			Error:  result.Error.Error(),
		}, result.Error
	}

	data := &pb.FindOneData{
		Id:    product.Id,
		Name:  product.Name,
		Stock: product.Stock,
		Price: product.Price,
	}

	return &pb.FindOneResponse{
		Status: http.StatusOK,
		Data:   data,
	}, nil
}

func (s *Server) DecreaseStock(ctx context.Context, req *pb.DecreaceStockRequest) (*pb.DecreaceStockResponse, error) {
	var product models.Product

	if result := s.H.DB.First(&product, req.Id); result.Error != nil {
		return &pb.DecreaceStockResponse{
			Status: http.StatusNotFound,
			Error:  result.Error.Error(),
		}, result.Error
	}

	if product.Stock <= 0 {
		return &pb.DecreaceStockResponse{
			Status: http.StatusConflict,
			Error:  "Stock too low",
		}, errors.New("Stock too low")
	}

	var log models.StockDecreaseLog

	if result := s.H.DB.Where(&models.StockDecreaseLog{OrderId: req.OrderId}).First(&log); result.Error != nil {
		return &pb.DecreaceStockResponse{
			Status: http.StatusConflict,
			Error:  result.Error.Error(),
		}, result.Error
	}

	product.Stock--

	s.H.DB.Save(&product)

	log.OrderId = req.OrderId
	log.ProductRefer = req.Id

	s.H.DB.Create(&log)

	return &pb.DecreaceStockResponse{
		Status: http.StatusOK,
	}, nil
}

func (s *Server) mustEmbedUnimplementedProductServiceServer() {}
