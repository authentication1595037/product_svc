package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/azonnix/authentication/product-svc/pkg/config"
	"gitlab.com/azonnix/authentication/product-svc/pkg/db"
	"gitlab.com/azonnix/authentication/product-svc/pkg/pb"
	"gitlab.com/azonnix/authentication/product-svc/pkg/services"
	"google.golang.org/grpc"
)

func main() {
	c, err := config.LoadConfig()
	if err != nil {
		log.Fatal("Failed to load config:", err.Error())
	}

	h, err := db.Init(c.DBUrl)
	if err != nil {
		log.Fatal("Failed to init db:", err.Error())
	}

	lis, err := net.Listen("tcp", c.Port)
	if err != nil {
		log.Fatal("Failed to listning:", err.Error())
	}

	fmt.Println("Product svc on:", c.Port)

	s := services.Server{
		H: h,
	}

	grpcServer := grpc.NewServer()

	pb.RegisterProductServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatal("Failed to serve:", err.Error())
	}
}
